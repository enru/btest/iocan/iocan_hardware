IOCAN 

universal IO CAN controller with 5 iso output and 5 iso  input on STM32G474CE

[schematic](doc/pcb/schematic.pdf)  
[enclosure](enclosure/)  
[gerber](doc/pcb/gerber/)

![](doc/flowchart.png)

PCB view 

![](doc/photo/top.png)

with enclosure 

![](doc/photo/maincut.png)
